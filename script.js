/*** REGION 1 - Global variables ***/
let fileInput = document.getElementById('file-input');
let containerCard = document.getElementById('card-container');
let numOfFiles = document.getElementById('num-of-files');

/*** REGION 2 - event functional */
/*** REGION 3 - Event handlers */

function preview() {
    // containerCard.innerHTML = " ";
    numOfFiles.textContent = `${fileInput.files.length} Files Selected`;

    for (let i of fileInput.files) {
        let reader = new FileReader();

        reader.onload = () => {
            let imgCard = document.createElement('div');
            imgCard.classList.add('img-card');

            let figure = document.createElement('figure');
            let figCap = document.createElement('figcaption');
            figCap.innerText = i.name;
            figure.appendChild(figCap);

            let img = document.createElement('img');
            img.setAttribute('src', reader.result);
            figure.insertBefore(img, figCap);

            imgCard.appendChild(figure);

            let iconList = document.createElement('div');
            iconList.classList.add('icon-list');

            let moveLeftBtn = document.createElement('button');
            moveLeftBtn.type = 'button';
            moveLeftBtn.innerHTML = '<i class="fas fa-arrow-left icon"></i>';
            moveLeftBtn.addEventListener('click', () => moveCardToLeft(imgCard));
            iconList.appendChild(moveLeftBtn);

            let moveRightBtn = document.createElement('button');
            moveRightBtn.type = 'button';
            moveRightBtn.innerHTML = '<i class="fas fa-arrow-right icon"></i>';
            moveRightBtn.addEventListener('click', () => moveCardToRight(imgCard));
            iconList.appendChild(moveRightBtn);

            let deleteBtn = document.createElement('button');
            deleteBtn.type = 'button';
            deleteBtn.innerHTML = '<i class="fas fa-trash-alt icon"></i>';
            deleteBtn.addEventListener('click', () => deleteCard(imgCard));
            iconList.appendChild(deleteBtn);

            imgCard.appendChild(iconList);

            containerCard.appendChild(imgCard);
        }

        reader.readAsDataURL(i);
    }
}

/*** REGION 4 - Common funtions */

function moveCardToLeft(card) {
    containerCard.insertBefore(card, card.previousSibling);
}

function moveCardToRight(card) {
    containerCard.insertBefore(card, card.nextSibling.nextSibling);
}

function deleteCard(card) {
    const confirmDelete = confirm('Are you sure you want to delete this card?');
    if (confirmDelete) {
      containerCard.removeChild(card);
    }
  }
///////////////////////////////////

function savePage() {
    // Lưu trạng thái hiện tại của trang
    let cardContainerHTML = containerCard.innerHTML;
    localStorage.setItem('savedPage', cardContainerHTML);
    alert('Pages state saved successful !');
}

// restore page
function restorePage() {
    let savedPage = localStorage.getItem('savedPage');
    if (savedPage) {
        containerCard.innerHTML = savedPage;
        attachEventListeners();
    }
}

// listen events to restore page
function attachEventListeners() {
    let moveLeftBtns = document.querySelectorAll('.icon-list button:first-child');
    moveLeftBtns.forEach((btn) => {
        btn.addEventListener('click', () => moveCardToLeft(btn.closest('.img-card')));
    });

    let moveRightBtns = document.querySelectorAll('.icon-list button:nth-child(2)');
    moveRightBtns.forEach((btn) => {
        btn.addEventListener('click', () => moveCardToRight(btn.closest('.img-card')));
    });

    let deleteBtns = document.querySelectorAll('.icon-list button:last-child');
    deleteBtns.forEach((btn) => {
        btn.addEventListener('click', () => deleteCard(btn.closest('.img-card')));
    });
}

// call savePage()
document.getElementById('save-button').addEventListener('click', savePage);

// reload page
window.addEventListener('load', restorePage);