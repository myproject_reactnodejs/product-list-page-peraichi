# Image Gallery Web Application

This is a simple web application for managing an image gallery. Users can upload images, view them in a gallery layout, move images left or right within the gallery, and delete images.

## Features

- **Image Upload**: Users can select and upload multiple images from their local filesystem.
- **Image Gallery**: Uploaded images are displayed in a gallery layout.
- **Navigation Buttons**: Each image card in the gallery has buttons to move the image left or right within the gallery.
- **Delete Images**: Users can delete images from the gallery by clicking on a delete button.
- **API Support**: The application supports various APIs for managing images, such as:
  - **Local Storage API**: Utilizes browser local storage for storing image data.
  - **Local Folder Access API**: Allows access to local folders for uploading images directly from the user's device.
  - **Custom Backend API**: Integration with a custom backend API for storing, retrieving, and managing images on a server.

## I took 1 day to complete all.

## Getting Started

To get started with the project, follow these steps:

1. Clone the repository to your local machine:

```bash
git clone <https://gitlab.com/myproject_reactnodejs/product-list-page-peraichi>

Open the index.html file in your web browser.

Usage

Uploading Images: Click on the "Choose images here" button to select one or more images from your local filesystem. The selected images will be displayed in the gallery.

Moving Images: To move an image within the gallery, click on the left or right arrow buttons on the image card.

Deleting Images: To delete an image from the gallery, click on the trash icon button on the image card. A confirmation dialog will appear before deleting the image.

Technologies Used
HTML
CSS
JavaScript
License
This project is licensed under the MIT License.